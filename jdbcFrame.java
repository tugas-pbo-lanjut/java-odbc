import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class jdbcFrame {

	
	private JFrame frame;
	private JTable table;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	DBBiodata biodata;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					jdbcFrame frame = new jdbcFrame();
					frame.setVisible(true);
					 System.out.println("frame berhasil;");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected void setVisible(boolean b) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Create the application.
	 */
	public jdbcFrame() {
		super();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		biodata = new DBBiodata();
		Vector<String> columnNames = new Vector<String>();
		columnNames.addElement("NIM");
		columnNames.addElement("NAMA");
		columnNames.addElement("JENIS KELAMIN");
		columnNames.addElement("TEMPAT LAHIR");
		columnNames.addElement("TANGGAL LAHIR");
		columnNames.addElement("ALAMAT");
		JTable table = new JTable(biodata.data(), columnNames);
		DefaultTableModel dtm=new DefaultTableModel(biodata.data(),columnNames);
		table.setModel(dtm);
		scrollPane.setColumnHeaderView(table);
	}

	private void setContentPane(JPanel contentPane2) {
		// TODO Auto-generated method stub
		
	}

	private void setBounds(int i, int j, int k, int l) {
		// TODO Auto-generated method stub
		
	}

	private void setDefaultCloseOperation(int exitOnClose) {
		// TODO Auto-generated method stub
		
	}
}
